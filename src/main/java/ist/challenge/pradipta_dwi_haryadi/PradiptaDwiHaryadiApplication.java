package ist.challenge.pradipta_dwi_haryadi;

import ist.challenge.pradipta_dwi_haryadi.configs.RSAKeyProperties;
import ist.challenge.pradipta_dwi_haryadi.configs.SwapiProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@EnableConfigurationProperties({RSAKeyProperties.class, SwapiProperties.class})
public class PradiptaDwiHaryadiApplication {

	public static void main(String[] args) {
		SpringApplication.run(PradiptaDwiHaryadiApplication.class, args);
	}

}
