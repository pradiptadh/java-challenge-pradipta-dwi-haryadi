package ist.challenge.pradipta_dwi_haryadi.controllers;

import ist.challenge.pradipta_dwi_haryadi.dtos.request.LoginRequestDTO;
import ist.challenge.pradipta_dwi_haryadi.dtos.request.RegisterRequestDTO;
import ist.challenge.pradipta_dwi_haryadi.dtos.request.UserRequestDTO;
import ist.challenge.pradipta_dwi_haryadi.dtos.response.BaseResponseDTO;
import ist.challenge.pradipta_dwi_haryadi.exceptions.UsernameOrPasswordEmptyException;
import ist.challenge.pradipta_dwi_haryadi.services.IAuthService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/auth")
public class AuthController {
    private final IAuthService authServices;

    private final AuthenticationManager authenticationManager;

    public AuthController(IAuthService authServices, AuthenticationManager authenticationManager) {
        this.authServices = authServices;
        this.authenticationManager = authenticationManager;
    }

    @PostMapping("/register")
    public ResponseEntity<BaseResponseDTO<String>> addUser(@RequestBody RegisterRequestDTO registerRequestDTO) {
        authServices.registerUser(registerRequestDTO);
        return new ResponseEntity<>(BaseResponseDTO.<String>builder()
                .status("Success")
                .message("User successfully created")
                .code(HttpStatus.CREATED.value())
                .build(), HttpStatus.CREATED);
    }

    @PostMapping("/login")
    public ResponseEntity<BaseResponseDTO<String>> token(@RequestBody LoginRequestDTO loginRequestDTO) {

        if (loginRequestDTO.getUsername().isEmpty() || loginRequestDTO.getPassword().isEmpty()) {
            throw new UsernameOrPasswordEmptyException("Username dan / atau password kosong");
        }

        Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(loginRequestDTO.getUsername(), loginRequestDTO.getPassword()));

        String token = authServices.generatedToken(authentication);

        return new ResponseEntity<>(BaseResponseDTO.<String>builder()
                .status("Success")
                .message("User successfully login")
                .code(HttpStatus.OK.value())
                .data(token)
                .build(), HttpStatus.OK);
    }

    @GetMapping("/info")
    public ResponseEntity<UserRequestDTO> getUserInfo(@RequestHeader(name ="Authorization") String bearerToken){
        UserRequestDTO userRequestDTO = authServices.decodeToken(bearerToken);
        return new ResponseEntity<>(userRequestDTO, HttpStatus.OK);

    }
}
