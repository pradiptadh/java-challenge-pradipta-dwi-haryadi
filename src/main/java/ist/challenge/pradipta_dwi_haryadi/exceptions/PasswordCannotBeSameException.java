package ist.challenge.pradipta_dwi_haryadi.exceptions;

public class PasswordCannotBeSameException extends RuntimeException{
    public PasswordCannotBeSameException(String message) {
        super(message);
    }
}
