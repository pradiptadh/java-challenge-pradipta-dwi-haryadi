package ist.challenge.pradipta_dwi_haryadi.exceptions;

public class PasswordNotValidException extends RuntimeException{
    public PasswordNotValidException(String message) {
        super(message);
    }
}
