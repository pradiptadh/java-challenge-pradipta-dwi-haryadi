package ist.challenge.pradipta_dwi_haryadi.exceptions;

public class UsernameFoundException extends RuntimeException{
    public UsernameFoundException(String message) {
        super(message);
    }
}
