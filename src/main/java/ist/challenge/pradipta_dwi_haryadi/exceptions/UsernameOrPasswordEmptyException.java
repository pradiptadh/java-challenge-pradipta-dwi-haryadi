package ist.challenge.pradipta_dwi_haryadi.exceptions;

public class UsernameOrPasswordEmptyException extends RuntimeException{
    public UsernameOrPasswordEmptyException(String message) {
        super(message);
    }
}
