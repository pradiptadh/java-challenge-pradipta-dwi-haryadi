package ist.challenge.pradipta_dwi_haryadi.services;

import ist.challenge.pradipta_dwi_haryadi.dtos.request.RegisterRequestDTO;
import ist.challenge.pradipta_dwi_haryadi.dtos.request.UserRequestDTO;
import ist.challenge.pradipta_dwi_haryadi.dtos.response.UserResponseDTO;
import ist.challenge.pradipta_dwi_haryadi.entities.Users;
import org.springframework.security.core.Authentication;

public interface IAuthService {

    public String generatedToken(Authentication authentication);

    public UserRequestDTO decodeToken(String token);

    UserResponseDTO registerUser(RegisterRequestDTO registerRequestDTO);

    Users findUser(String username);
}
