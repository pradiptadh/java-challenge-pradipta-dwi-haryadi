package ist.challenge.pradipta_dwi_haryadi.services;

import ist.challenge.pradipta_dwi_haryadi.dtos.response.ResultDTO;
import ist.challenge.pradipta_dwi_haryadi.dtos.response.SwapiResponseDTO;

public interface ISwapiDBService {

    SwapiResponseDTO searchPeopleOnSwapi(String name);

    ResultDTO getDetailPeopleByIdUrl(Long id);


}
