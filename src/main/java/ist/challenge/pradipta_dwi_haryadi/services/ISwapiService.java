package ist.challenge.pradipta_dwi_haryadi.services;

import ist.challenge.pradipta_dwi_haryadi.dtos.response.PaginationPeopleSwapiResponseDTO;
import ist.challenge.pradipta_dwi_haryadi.dtos.response.ResultDTO;
import org.springframework.data.domain.Pageable;

public interface ISwapiService {

    ResultDTO addPeople(Long id);

    PaginationPeopleSwapiResponseDTO getAllPeopleSwapi(String query, Pageable pageable);
}
