package ist.challenge.pradipta_dwi_haryadi.services;

import ist.challenge.pradipta_dwi_haryadi.dtos.request.UserRequestDTO;
import ist.challenge.pradipta_dwi_haryadi.dtos.response.PaginationAllUserResponseDTO;
import ist.challenge.pradipta_dwi_haryadi.dtos.response.UserUsernameResponseDTO;
import org.springframework.data.domain.Pageable;

public interface IUserService {
    UserUsernameResponseDTO updateUser(String username , UserRequestDTO userRequestDTO);
    UserUsernameResponseDTO getUser(String username);

    PaginationAllUserResponseDTO getAllUser(String query, Pageable pageable);
}
