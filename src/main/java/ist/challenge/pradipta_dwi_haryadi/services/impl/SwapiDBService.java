package ist.challenge.pradipta_dwi_haryadi.services.impl;

import ist.challenge.pradipta_dwi_haryadi.configs.SwapiProperties;
import ist.challenge.pradipta_dwi_haryadi.dtos.response.ResultDTO;
import ist.challenge.pradipta_dwi_haryadi.dtos.response.SwapiResponseDTO;
import ist.challenge.pradipta_dwi_haryadi.services.ISwapiDBService;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;

@Service
public class SwapiDBService implements ISwapiDBService {

    private final RestTemplate restTemplate;
    private final SwapiProperties swapiProperties;

    public SwapiDBService(RestTemplate restTemplate, SwapiProperties swapiProperties) {
        this.restTemplate = restTemplate;
        this.swapiProperties = swapiProperties;
    }


    @Override
    public SwapiResponseDTO searchPeopleOnSwapi(String name) {
        Map<String, String> uriVariables = new HashMap<>();
        uriVariables.putIfAbsent("name", name);
        return restTemplate.getForObject(
                swapiProperties.getUrl() + "/people/?search={name}",
                SwapiResponseDTO.class,
                uriVariables
        );
    }

    @Override
    public ResultDTO getDetailPeopleByIdUrl(Long id) {
        Map<String, String> uriVariables = new HashMap<>();
        uriVariables.putIfAbsent("id", id.toString());

        return restTemplate.getForObject(
                swapiProperties.getUrl() + "/people/{id}",
                ResultDTO.class,
                uriVariables
        );
    }

}
